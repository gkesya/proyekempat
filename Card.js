import React from 'react';
import PropTypes from 'prop-types';

function Card(props) {
  const { title, image } = props;

  return (
    <div className="card">
      <img src={image} alt={title} />
      <h3>{title}</h3>
    </div>
  );
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default Card;