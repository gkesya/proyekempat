import React, { useState, useEffect } from 'react';
import { useAsync } from 'react-use';
import axios from 'axios';

function useGetApi(url) {
  const { value: data, loading, error, execute } = useAsync(async () => {
    const response = await axios.get(url);
    return response.data;
  }, [url]);

  useEffect(() => {
    execute();
  }, [execute]);

  return {
    data,
    loading,
    error,
  };
}

function App() {
  const { data, loading, error } = useGetApi('https://example.com/api');

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div>
      {data && (
        <ul>
          {data.map((item) => (
            <li key={item.id}>{item.name}</li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default App;