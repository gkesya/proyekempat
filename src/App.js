import React from 'react';

function createTheme(primaryColor) {
  return {
    colorPrimary: primaryColor,
  };
}

function App() {
  const primaryColor = process.env.REACT_APP_COLOR_PRIMARY;
  const theme = createTheme(primaryColor);

  return (
    <div>
      <nav>
        <ul>
          <li>My App</li>
          <li>About</li>
          <li>Memory</li>
          <li className="right">Login</li>
        </ul>
      </nav>
      <div className="content">
        <div className="food">
          <img src="gambar1.jpg" alt="Makanan 1" />
          <h3>Judul Makanan 1</h3>
        </div>
        <div className="food">
          <img src="gambar2.jpg" alt="Makanan 2" />
          <h3>Judul Makanan 2</h3>
        </div>
        <div className="food">
          <img src="gambar3.jpg" alt="Makanan 3" />
          <h3>Judul Makanan 3</h3>
        </div>
        <div className="food">
          <img src="gambar4.jpg" alt="Makanan 4" />
          <h3>Judul Makanan 4</h3>
        </div>
        <div className="menu">
          <ul>
            <li>Kalender</li>
            <li>Topik</li>
            <li>Weather</li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;